import Axios from "axios";
import { API_URL, API_KEY } from "../config";

export const axios = Axios.create({
  baseURL: API_URL,
  headers: {
    "X-API-KEY": API_KEY,
  },
});
