import React, { useState } from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { PopulationData } from "./hooks/usePopulationData";
import { Line } from "react-chartjs-2";
import { LABELS } from "../constants/label";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

interface PopulationChartProps {
  data: PopulationData[][];
  prefNames: string[];
}

const PopulationChart: React.FC<PopulationChartProps> = ({
  data,
  prefNames,
}) => {
  const [selectedLabel, setSelectedLabel] = useState(LABELS.TOTAL);

  const chartData = {
    labels: data[0]?.[0]?.data.map((item) => item.year) || [],
    datasets: data.flatMap((prefData, index) => {
      const selectedData = prefData.find(
        (datum) => datum.label === selectedLabel
      );

      if (!selectedData) {
        return [];
      }
      return {
        label: `${prefNames[index]}`,
        data: selectedData.data.map((d) => d.value),
        fill: false,
        backgroundColor: `hsla(${
          (360 / prefNames.length) * index
        }, 100%, 50%, 0.3)`,
        borderColor: `hsla(${
          (360 / prefNames.length) * index
        }, 100%, 50%, 0.3)`,
        tension: 0.1,
      };
    }),
  };

  return (
    <div>
      <div>
        {Object.values(LABELS).map((label) => (
          <button
            key={label}
            onClick={() => setSelectedLabel(label)}
            style={selectedLabel === label ? { fontWeight: "bold" } : {}}
          >
            {label}
          </button>
        ))}
      </div>
      <div style={{ width: "1000px" }}>
        <Line data={chartData} />
      </div>
    </div>
  );
};

export default PopulationChart;
