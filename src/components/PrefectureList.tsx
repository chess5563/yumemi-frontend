import React, { useState } from "react";
import { usePrefectures } from "./hooks/usePrefectures";
import { usePopulationData } from "./hooks/usePopulationData";
import PopulationChart from "./PrefectureChart";

type CheckBoxChangeEvent = React.ChangeEvent<HTMLInputElement>;

const PrefectureList: React.FC = () => {
  const { data: prefectures, isLoading, isError } = usePrefectures();
  const [selectedPrefs, setSelectedPrefs] = useState<number[]>([]);
  const [selectedPrefNames, setSelectedPrefNames] = useState<string[]>([]);
  const {
    data: populationData,
    isLoading: isPopulationLoading,
    isError: isPopulationError,
  } = usePopulationData(selectedPrefs);

  if (isLoading) return <div>Loading...</div>;
  if (isError) return <div>Error loading data.</div>;

  if (isPopulationError) return <div>Error loading population data.</div>;

  const handleCheck = (
    event: CheckBoxChangeEvent,
    prefCode: number,
    prefName: string
  ) => {
    if (event.target.checked) {
      setSelectedPrefs([...selectedPrefs, prefCode]);
      setSelectedPrefNames([...selectedPrefNames, prefName]);
    } else {
      setSelectedPrefs(selectedPrefs.filter((code) => code !== prefCode));
      const nameIndex = selectedPrefNames.findIndex(
        (name) => name === prefName
      );
      const newPrefNames = [...selectedPrefNames];
      newPrefNames.splice(nameIndex, 1);
      setSelectedPrefNames(newPrefNames);
    }
  };

  return (
    <div className="prefecture-container">
      <h2 className="header">都道府県</h2>
      <div className="prefecture-list">
        {prefectures?.map((prefecture) => (
          <div key={prefecture.prefCode} className="prefecture-item">
            <input
              type="checkbox"
              id={`pref-${prefecture.prefCode}`}
              onChange={(event) =>
                handleCheck(event, prefecture.prefCode, prefecture.prefName)
              }
            />
            <label htmlFor={`pref-${prefecture.prefCode}`}>
              {prefecture.prefName}
            </label>
          </div>
        ))}
      </div>
      {populationData && (
        <PopulationChart data={populationData} prefNames={selectedPrefNames} />
      )}
    </div>
  );
};

export default PrefectureList;
