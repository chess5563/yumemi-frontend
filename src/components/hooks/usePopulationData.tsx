import useSWR from "swr";
import { axios } from "../../lib/axios";
import { z } from "zod";

interface PopulationDataPoint {
  year: number;
  value: number;
}

export interface PopulationData {
  label: string;
  data: PopulationDataPoint[];
}

const PopulationResponseSchema = z.object({
  result: z.object({
    data: z.array(
      z.object({
        label: z.string(),
        data: z.array(
          z.object({
            year: z.number(),
            value: z.number(),
          })
        ),
      })
    ),
  }),
});

const fetcher = (url: string, params?: Record<string, any>) =>
  axios.get(url, { params }).then((res) => res.data);

const fetchPopulationData = async (
  selectedPrefs: number[]
): Promise<PopulationData[][]> => {
  const results = await Promise.all(
    selectedPrefs.map((prefCode) =>
      fetcher("/population/composition/perYear", {
        prefCode: prefCode,
        cityCode: "-",
      })
    )
  );
  return results.map((result) => {
    const parsed = PopulationResponseSchema.safeParse(result);
    if (!parsed.success) {
      throw new Error(`Validation error for prefCode.`);
    }
    return parsed.data.result.data;
  });
};

export const usePopulationData = (selectedPrefs: number[]) => {
  const { data, error } = useSWR(
    selectedPrefs.length > 0 ? ["populationData", ...selectedPrefs] : null,
    () => fetchPopulationData(selectedPrefs)
  );

  return {
    data: data || [],
    isLoading: !error && !data,
    isError: error,
  };
};
