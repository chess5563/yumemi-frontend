import { axios } from "../../lib/axios";
import { z } from "zod";
import useSWR from "swr";

interface Prefecture {
  prefCode: number;
  prefName: string;
}

const PrefecturesResponseSchema = z.object({
  result: z.array(
    z.object({
      prefCode: z.number(),
      prefName: z.string(),
    })
  ),
});

const fetcher = (url: string, params?: Record<string, any>) =>
  axios.get(url, { params }).then((res) => res.data);

export const usePrefectures = () => {
  const { data, error } = useSWR<{ result: Prefecture[] }>(
    "/prefectures",
    fetcher,
    {
      onSuccess: (data) => {
        try {
          PrefecturesResponseSchema.parse(data);
        } catch (error) {
          throw new Error("Failed to parse prefectures data.");
        }
      },
    }
  );

  return {
    data: data?.result,
    isLoading: !error && !data,
    isError: error,
  };
};
