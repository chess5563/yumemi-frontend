import "./App.scss";
import PrefectureList from "./components/PrefectureList";

function App() {
  return (
    <div className="App">
      <h2>Yumemi Frontend</h2>
      <PrefectureList />
    </div>
  );
}

export default App;
