export const LABELS = {
  TOTAL: "総人口",
  YOUNG: "年少人口",
  PRODUCTIVE: "生産年齢人口",
  OLD: "老年人口",
};
