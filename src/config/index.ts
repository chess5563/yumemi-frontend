export const API_URL = process.env.REACT_APP_RESAS_API_BASE_URL as string;
export const API_KEY = process.env.REACT_APP_RESAS_API_KEY as string;
